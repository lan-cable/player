const http = require("http");

const defaultArguments = [
  "--fullscreen",
  "--no-video-title-show",
  "--no-osd",
  "--no-snapshot-preview",
  "--no-loop",
  "--no-repeat",
];

const { spawn } = require("child_process");

class VLC {
  constructor(filePath, { password, port, arguments: args, vlcPath } = {}) {
    if (typeof filePath !== "string") {
      throw new TypeError("Please provide a path to a file");
    }

    this.filePath = filePath;
    this.password = password;
    this.port = port;
    this.arguments = args || defaultArguments;

    if (typeof vlcPath !== "string") {
      throw new TypeError("Please provide a path to the VLC executable");
    }

    this.callbacks = {};
    this.interval = null;

    this.process = spawn(vlcPath, this.getArguments());

    VLC.#players.push(this);
    this.setupStatusRequests();
  }

  getArguments() {
    return [
      ...this.arguments,
      `--http-port=${this.port}`,
      `--http-password=${this.password}`,
      "--extraintf=http",
      this.filePath,
    ];
  }

  on(what, cb) {
    if (!cb || typeof cb !== "function") {
      throw new TypeError(
        `Please provide a callback function for the '${what}' event`
      );
    }
    this.callbacks[what] = cb;
  }

  kill() {
    // Kill previous process
    if (this.process) {
      this.process.kill("SIGKILL");
    }
  }

  async request(endpoint, cb) {
    if (typeof endpoint !== "string" || typeof cb !== "function") {
      throw new TypeError(
        "Please provide a valid endpoint string and a callback function"
      );
    }
    const url = `http://:${this.password}@localhost:${this.port}${endpoint}`;
    http
      .get(url, (response) => {
        let data = "";
        response.on("data", (chunk) => (data += chunk));
        response.on("end", () => {
          let jsonData;
          try {
            jsonData = JSON.parse(data);
          } catch (error) {
            cb(error);
            return;
          }
          cb(null, jsonData);
        });
      })
      .on("error", (error) => cb(error));
  }

  setupStatusRequests(interval = 300) {
    this.interval = setInterval(async () => {
      if (!this.callbacks.statuschange) return;
      await this.request("/requests/status.json", (error, status) => {
        if (error) {
          this.callbacks.statuschange(error, status);
          return;
        }
        if (!status || !status.time) {
          this.callbacks.statuschange("unexpected HTTP request error");
          return;
        }
        if (this.previousTime === status.time) return;
        this.callbacks.statuschange(null, status);
        this.previousTime = status.time;
      });
    }, interval);
  }

  quit() {
    if (this.process) {
      this.process.kill("SIGKILL");
    }
  }

  static get players() {
    return VLC.#players;
  }

  static #players = [];
}

process.on("SIGINT", function () {
  VLC.players.forEach((player) => player.quit());
  process.exit();
});

module.exports = VLC;
