const pino = require("pino");
const logger = pino(); // Create a logger instance using the pino library
const CecController = require("cec-controller");
const cecCtl = new CecController(); // Create a new instance of the CEC controller

/**
 * Module for handling video playback and socket communication.
 */

// Import necessary modules.
const VLC = require("./player"); // Import the VLC player module
const io = require("socket.io-client"); // Import the socket.io client module
require("dotenv").config(); // Load environment variables from .env file

// Connect to the API server and listen for play events.
const socket = io.connect(process.env.API_URL, {
  // Add the player token as authentication.
  auth: {
    token: process.env.PLAYER_TOKEN,
  },
});

/**
 * Event listener for the CEC controller's 'ready' event.
 * @param {Object} controller - The CEC controller object.
 */
cecCtl.on("ready", (controller) => {
  cecHandler(controller); // Handle the 'ready' event
});

/**
 * Event listener for the CEC controller's 'error' event.
 * @param {Error} error - The error object.
 */
cecCtl.on("error", (error) => {
  logger.error(`Error occurred: ${error}`);
});

/**
 * Event listener for the CEC controller's 'keypress' event.
 * @param {string} key - The key that was pressed.
 */
function cecHandler(controller) {
  controller.on("keypress", (key) => {
    logger.info(`Received CEC keypress: ${key}`);
    socket.emit("keypress", key); // Emit the keypress event to the server
  });
}

let playerInstance; // Variable to hold the current player instance
let currentlyPlaying; // Variable to hold the currently playing video URL

/**
 * Event listener for the socket.io 'connect' event.
 */
socket.on("connect", () => {
  if (!playerInstance) {
    socket.emit("statuschange", {
      status: {
        state: "connected",
      },
    });
  }
});

/**
 * Event listener for the 'play' event.
 * @param {string} videoUrl - The URL of the video to play.
 */
socket.on("play", (videoUrl) => {
  logger.info(`Received play event for video: ${videoUrl}`);

  if (currentlyPlaying === videoUrl) {
    logger.info(`Video ${videoUrl} is already playing`);
    return; // If the provided video URL is the same as the currently playing video, do nothing
  }

  currentlyPlaying = videoUrl;

  // Kill the current player instance.
  playerInstance?.kill(); // Kill the current player instance if it exists

  // Create a new VLC player instance.
  logger.info(`Creating new player instance for video: ${videoUrl}`);
  playerInstance = new VLC(videoUrl, {
    password: process.env.VLC_PASSWORD, // Set the password
    port: 8080, // Set the port to 8080.
    vlcPath: process.env.VLC_PATH, // Set the VLC path.
  });

  // Track the last time the statuschange event was emitted.
  let lastEmitted = 0;

  /**
   * Event listener for the 'statuschange' event.
   * @param {string} error - The error message if there was an error.
   * @param {object} status - The status object containing information about the player.
   */
  playerInstance.on("statuschange", (error, status) => {
    // If it's been more than a second since the last emission, emit the status.
    if (Date.now() - lastEmitted > 1000) {
      logger.debug(`Emitting statuschange event for video: ${videoUrl}`);
      socket.emit("statuschange", { error, status }); // Emit the status to the server
      lastEmitted = Date.now(); // Update the lastEmitted timestamp
    }
  });

  /**
   * Event listener for the 'error' event.
   * @param {Error} error - The error object.
   */
  playerInstance.on("error", (error) => {
    logger.error(`Error occurred in player instance: ${error}`);
  });
});

